import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-material-escolar',
  templateUrl: './material-escolar.component.html',
  styleUrls: ['./material-escolar.component.css']
})
export class MaterialEscolarComponent implements OnInit {

  form!: FormGroup;
  checked: boolean = false;
  form1!: FormGroup;
  lista!: string;
  entra: string[] = [];
  lista2!: string;
  borradoAuxiliar:string = '';

  constructor(
    private fb: FormBuilder

  ) { 
    this.crearFormulario();
  }

  ngOnInit(): void {
  }


  crearFormulario(): void {
    this.form = this.fb.group(
      {
        id: [''],
        // aceptar: ['false'],
        utiles: this.fb.array([[this.form1 = this.fb.group({
          aceptar: ['',],
          nombre: ['', [Validators.pattern(/^[0-9a-zA-zñÑ\s]+$/)]],
          // textarea: ['']
        })
        ]]),
      }
    )
  }


  // Para verificar si los carácteres son validos
  get nombreNoValido() {
    // si al obtener nombre este es invalido y es touched aplicaremos una clase de css en el html
    // el signo '?' de interrogación indica que sea opcional ya que al principio se muestra una casilla vacia ''
    return (
      this.form1.get('nombre')?.invalid && this.form1.get('nombre')?.touched
    );
  }

  get utilesEscolares() {
    return this.form.get('utiles') as FormArray;
  }

  agregarUtil(): void {
    this.utilesEscolares.push(this.fb.control(''))
    this.form1.patchValue({
      nombre: ''
    })
    
  }

  eliminarUtil(id: number): void {
    this.utilesEscolares.removeAt(id)
    /* this.form1.reset({
      nombre: ''
    }) */
  }

  eliminarUtiles(): void {
    // volvemos los valores a un array vacio
    this.form = this.fb.group(
      {
        utiles: this.fb.array([])
      }
    )
  }

  limpiarCajas(): void { 
    // volvemos el valor de los input vacias  
   this.form1.reset({
     nombre: ''
   })
   this.entra = [];
   this.lista2 = this.entra.toString();
  }


 guardarCajas(): void {

    // verificamos is el checkbox esta seleccionado
    if (this.form1.value.aceptar === true) {
      // obtenemos le valor del nombre del input
      this.lista = this.form1.value.nombre;
      // guardamos en un array cada nombre
      this.entra.push(this.lista)
      
      // convertimos el array en un string y lo separamos con un salto de linea
      this.lista2 = this.entra.join('\n')

      // lo volvemos en un array separado por saltos de linea y luego lo unimos con un br para darle su salto en el html
      this.lista2 = this.lista2.split("\n").join("<br>")
      
      // descheckea el checkbox para que no lo añada de nuevo
      this.form1.patchValue({
        aceptar: ''
      })
    }
  }
}
